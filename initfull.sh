cd ..
git clone git@bitbucket.org:datafundamentals/smoslt.analytics.git
git clone git@bitbucket.org:datafundamentals/smoslt.app.git
git clone git@bitbucket.org:datafundamentals/smoslt.domain.git
git clone git@bitbucket.org:datafundamentals/smoslt.generator.git
git clone git@bitbucket.org:datafundamentals/smoslt.given.git
git clone git@bitbucket.org:datafundamentals/smoslt.main.git
git clone git@bitbucket.org:datafundamentals/smoslt.mprtxprt.git
git clone git@bitbucket.org:datafundamentals/smoslt.options.git
git clone git@bitbucket.org:datafundamentals/smoslt.optionsapi.git
git clone git@bitbucket.org:datafundamentals/smoslt.poms.git
git clone git@bitbucket.org:datafundamentals/smoslt.scoremunger.git
git clone git@bitbucket.org:datafundamentals/smoslt.stacker.git
git clone git@bitbucket.org:datafundamentals/smoslt.util.git
git clone git@bitbucket.org:datafundamentals/smoslt.workspace_root.git
git clone git@bitbucket.org:datafundamentals/smoslt.webgen.git
git clone git@bitbucket.org:betterology/org.btrg.uti.git
cd smoslt.webgen
mvn clean install -DskipTests
cd ..
cd smoslt.poms
mvn clean install -DskipTests
cd ..
cd org.btrg.uti
mvn clean install -DskipTests
cd ..
cd smoslt.workspace_root
mvn clean install -DskipTests
cd ..
ls
echo "Congrats, your workspace is built, if you see BUILD SUCCESS above"
echo "Now, import all these projects into a fresh Elipse Luna workspace"
