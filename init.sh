cd ..
git clone git@bitbucket.org:datafundamentals/smoslt.given.git
git clone git@bitbucket.org:datafundamentals/smoslt.main.git
cd smoslt.given
mvn clean install -DskipTests
cd ../smoslt.main
mvn clean install -DskipTests
cd ..
ls
echo "Congrats, your light workspace is built, if you see BUILD SUCCESS above"
echo "Now, import these two projects into a fresh Elipse Luna workspace with their parent folder as the root of the workspace"
